/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.inheritanceshape;

/**
 *
 * @author OS
 */
public class Rectangle extends Shape{

    protected double wide;
    protected double length;

    public Rectangle(String name, double wide, double length) {
        super(name);
        this.wide = wide;
        this.length = length;
    }

    public double calArea() {
        return wide * length;
    }

    public double getWide() {
        return wide;
    }

    public double getLength() {
        return length;
    }

    @Override
    public void ShowArea() {
        if (wide <= 0 || length <= 0) {
            System.out.println("Wide and Length must more than zero!!!!");
        } else {
            System.out.println("Area of " + name + " : " + calArea());
        }
    }
}
