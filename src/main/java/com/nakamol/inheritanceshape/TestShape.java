/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.inheritanceshape;

/**
 *
 * @author OS
 */
public class TestShape {
    public static void main(String[] args) {
        Shape shape = new Shape("Shape");
        shape.calArea();
        shape.ShowArea();
        
        Circle circle1 = new Circle("Circle1", 3);
        circle1.calArea();
        circle1.ShowArea();
        
        Circle circle2 = new Circle("circle2", 4);
        circle2.calArea();
        circle2.ShowArea();
        
        Rectangle rectangle = new Rectangle("rectangle", 3, 4);
        rectangle.calArea();
        rectangle.ShowArea();
        
        Triangle triangle = new Triangle("triangle", 3, 4);
        triangle.calArea();
        triangle.ShowArea();
        
        Square square = new Square("square", 2);
        square.calArea();
        square.ShowArea();
    }
}
