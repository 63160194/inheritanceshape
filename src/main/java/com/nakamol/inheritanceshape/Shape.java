/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.inheritanceshape;

/**
 *
 * @author OS
 */
public class Shape {
    protected String name;
    
    
    public Shape(String name){
        this.name = name;
    }
    
    public double calArea(){
        return 0;
    }

    public void ShowArea() {
        System.out.println("Area of " + name + " : " + calArea());
    }
}
