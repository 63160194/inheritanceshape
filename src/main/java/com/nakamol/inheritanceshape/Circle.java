/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.inheritanceshape;

/**
 *
 * @author OS
 */
public class Circle extends Shape {

    private double r;
    private double pi = 22.0 / 7;

    public Circle(String name, double r) {
        super(name);
        this.r = r;
    }

    @Override
    public double calArea() {
        return pi * r * r;
    }

    public double getR() {
        return r;
    }

    public double getpi() {
        return pi;
    }

    @Override
    public void ShowArea() {
        if (r <= 0) {
            System.out.println("Redius must more than zero!!!!");
        } else {
            System.out.println("Area of " + name + " : " + calArea());
        }
    }
}
