/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.inheritanceshape;

/**
 *
 * @author OS
 */
public class Triangle extends Shape {

    private double base;
    private double heigth;

    public Triangle(String name, double base, double heigth) {
        super(name);
        this.base = base;
        this.heigth = heigth;
    }

    @Override
    public double calArea() {
        return 0.5 * base * heigth;
    }

    @Override
    public void ShowArea() {
        if (base <= 0 || heigth <= 0) {
            System.out.println("Base and Heigth must more than zero!!!!");
        } else {
            System.out.println("Area of " + name + " : " + calArea());
        }
    }
}
