/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nakamol.inheritanceshape;

/**
 *
 * @author OS
 */
public class Square extends Rectangle {

    private double side;

    public Square(String name, double side) {
        super(name, side, side);
        this.side = side;
    }

    @Override
    public void ShowArea() {
        if (side <= 0) {
            System.out.println("Side must more than zero!!!!");
        } else {
            System.out.println("Area of " + name + " : " + calArea());
        }
    }
}
